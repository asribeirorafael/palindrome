$(document).keypress(function(e) {
    if(e.which == 13) {

        var palindromeword = (retira_acentos(document.getElementById("pword").value)).toLowerCase().replace(/ /g,''),
            palindromeverified = ""


        for(var i = palindromeword.length; i > 0; i--){
            palindromeverified += palindromeword[i - 1];
        }

        if(palindromeverified == palindromeword){
            document.getElementById("msgPalindrome").style.display = "block";
            document.getElementById("msgNotPalindrome").style.display = "none";
        }else{
            document.getElementById("msgPalindrome").style.display = "none";
            document.getElementById("msgNotPalindrome").style.display = "block";
        }
    }
});

function retira_acentos(palavra) {
    var com_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ",
        sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC",
        nova = "";

    for(var i = 0;i<palavra.length;i++) {
        if (com_acento.search(palavra.substr(i,1))>=0) {
            nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1);
        }
        else {
            nova+=palavra.substr(i,1);
        }
    }
    return nova;
}